# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import os, site, re, yaml
from os.path import basename
from pathlib import Path
from textwrap import dedent
from xml.etree import ElementTree as ET
from dataclasses import dataclass
from typing import Dict, Collection, cast

from doit import get_var
from doit.action import BaseAction, CmdAction
from doit.tools import create_folder, run_once

from pdkmaster.typing import GDSLayerSpecDict
from pdkmaster.task import OpenPDKTree
from pdkmaster.technology import technology_ as _tch
from pdkmaster.design import library as _lbry

from pdkmaster.io.spice import SpicePrimsParamSpec, SpiceNetlistFactory, TaskManager as SpiceTaskManager
from pdkmaster.io.klayout import TaskManager as KLayoutTaskManager
from pdkmaster.io.tasyag import LibertyCornerDataT, TaskManager as TasYagTaskManager

import pdkmaster.technology, pdkmaster.design, pdkmaster.dispatch
import pdkmaster.io.spice, pdkmaster.io.klayout
import c4m, c4m.flexcell

### Config

DOIT_CONFIG = {
    "default_tasks": [
        "open_pdk", "gds", "spice", "klayout", "coriolis",
    ],
}


### support functions


def get_var_env(name, default=None):
    """Uses get_var to get a command line variable, also checks
    environment variables for default value

    If os.environ[name.upper()] exists that value will override the
    default value given.
    """
    try:
        default = os.environ[name.upper()]
    except:
        # Keep the specified default
        pass
    return get_var(name, default=default)


class AVTScriptAction(BaseAction):
    def __init__(self, avt_script, tmp=None):
        if tmp is None:
            tmp = tmp_dir
        self.script = avt_script
        self.tmp = tmp

        self.out = None
        self.err = None
        self.result = None
        self.values = {}

    def execute(self, out=None, err=None):
        # Create new action on every new call so we can always write
        # the script to the stdin of the subprocess.
        if avt_shell is None:
            action = CmdAction('echo "disabled because lack of avt_shell"')
        else:
            pr, pw = os.pipe()
            fpw = os.fdopen(pw, "w")
            fpw.write(self.script)
            fpw.close()

            action = CmdAction(avt_shell, stdin=pr, cwd=self.tmp)

        r = action.execute(out=out, err=err)
        self.values = action.values
        self.result = action.result
        self.out = action.out
        self.err = action.err
        return r


def get_tech() -> _tch.Technology:
    from c4m.pdk import gf180mcu
    return gf180mcu.tech
def get_lib(lib_name: str) -> _lbry.Library:
    from c4m.pdk import gf180mcu
    return cast(_lbry.Library, getattr(gf180mcu, lib_name.lower()))
def get_spiceprims() -> SpicePrimsParamSpec:
    from c4m.pdk import gf180mcu
    return gf180mcu.prims_spiceparams
def get_netlistfab() -> SpiceNetlistFactory:
    from c4m.pdk import gf180mcu
    return gf180mcu.netlistfab
def get_gdslayers() -> GDSLayerSpecDict:
    from c4m.pdk import gf180mcu
    return gf180mcu.gds_layers


### globals

def _first(it):
    return next(iter(it))

top_dir = Path(__file__).parent
tmp_dir = top_dir.joinpath("tmp")
setup_file = top_dir.joinpath("setup.py")

dist_dir = top_dir.joinpath("dist")

openpdk_tree = OpenPDKTree(top=top_dir.joinpath("open_pdk"), pdk_name="C4M.GF180MCU")

override_dir = top_dir.joinpath("override")

c4m_local_dir = top_dir.joinpath("c4m")
gf180mcu_local_dir = c4m_local_dir.joinpath("pdk", "gf180mcu")
c4m_inst_dir = Path(site.getsitepackages()[0]).joinpath("c4m")
gf180mcu_inst_dir = c4m_inst_dir.joinpath("pdk", "gf180mcu")
flexcell_inst_dir = Path(c4m.flexcell.__file__).parent

c4m_pdk_gf180mcu_py_files = tuple(gf180mcu_local_dir.rglob("*.py"))
pdkmaster_deps = (
    *Path(_first(pdkmaster.technology.__path__)).rglob("*.py"),
    *Path(_first(pdkmaster.design.__path__)).rglob("*.py"),
    *Path(_first(pdkmaster.dispatch.__path__)).rglob("*.py"),
)
pdkmaster_io_spice_deps = (
    *Path(_first(pdkmaster.io.spice.__path__)).rglob("*.py"),
)
pdkmaster_io_klayout_deps = (
    *Path(_first(pdkmaster.io.klayout.__path__)).rglob("*.py"),
)
flexcell_deps = (
    *Path(_first(c4m.flexcell.__path__)).rglob("*.py"),
)


# variables
python = get_var_env("python", default="python3")
pip = get_var_env("pip", default="pip3")

volare_rootdir = top_dir.joinpath(".volare")
# Set PDK_ROOT env. variable for volare
os.environ["PDK_ROOT"] = str(volare_rootdir)
gf180mcu_pdk_dir = volare_rootdir.joinpath("gf180mcuC")

avertec_top = get_var_env("avertec_top")
avt_shell = get_var_env(
    "avt_shell", default=(
        f"{avertec_top}/bin/avt_shell" if avertec_top is not None else None
    ),
)


### cell list


cell_list_file = top_dir.joinpath("cell_list.yml")

def task_cell_list():
    """Regenerate cell list.

    This task is not run by default. It needs to be run manually when the cell list
    has been changed and then the updated file has to be commit to git.
    """
    def write_list():
        import yaml

        from c4m.pdk import gf180mcu

        cell_list = {
            lib.name: list(cell.name for cell in lib.cells)
            for lib in gf180mcu.libs
        }
        with cell_list_file.open("w") as f:
            yaml.dump(cell_list, f)

    return {
        "title": lambda _: "Creating cell list file",
        "targets": (
            cell_list_file,
        ),
        "actions": (
            write_list,
        ),
    }

# We assume that the cell list is stored in git and is available in the top directory.
assert cell_list_file.exists()
with cell_list_file.open("r") as f:
    cell_list: Dict[str, Collection[str]]
    cell_list = yaml.safe_load(f)

lib_deps = {
    "StdCellLib": (*pdkmaster_deps, *flexcell_deps),
    "StdCell5V0Lib": (*pdkmaster_deps, *flexcell_deps),
}


### main tasks

#
# volare upstream Sky130 PDK
def task_volare():
    volare_hash = "6d4d11780c40b20ee63cc98e645307a9bf2b2ab8"

    return {
        "uptodate": (
            run_once,
        ),
        "actions": (
            f"volare enable --pdk gf180mcu {volare_hash}",
        ),
        "targets": (
            volare_rootdir.joinpath("volare", "gf180mcu", "versions", volare_hash),
        )
    }

#
# open_pdk
task_open_pdk = openpdk_tree.task_func


#
# spice_models
open_pdk_spice_dir = openpdk_tree.tool_dir(tool_name="ngspice")
spice_pdk_files = ("design.ngspice", "sm141064.ngspice")
spice_models_all_lib = open_pdk_spice_dir.joinpath("all.spice")
spice_models_tgts = (
    *(open_pdk_spice_dir.joinpath(file) for file in spice_pdk_files),
    spice_models_all_lib,
)
def task_spice_models():
    "Copy and generate C4M version of the models"
    gf180mcu_pdk_spice_dir = gf180mcu_pdk_dir.joinpath("libs.tech", "ngspice")

    def write_all():
        with spice_models_all_lib.open("w") as f:
            f.write(dedent("""
                * All corners file
                .lib init
                .include "design.ngspice"
                .endl

                .lib typical
                .lib "sm141064.ngspice" typical
                .endl

                .lib ff
                .lib "sm141064.ngspice" ff
                .endl

                .lib ss
                .lib "sm141064.ngspice" ss
                .endl

                .lib fs
                .lib "sm141064.ngspice" fs
                .endl

                .lib sf
                .lib "sm141064.ngspice" sf
                .endl
            """[1:]))

    return {
        "task_dep": (
            "volare",
        ),
        "file_dep": tuple(
            gf180mcu_pdk_spice_dir.joinpath(file) for file in spice_pdk_files
        ),
        "targets": spice_models_tgts,
        "actions": (
            (create_folder, (open_pdk_spice_dir,)),
            *(
                f"cp {str(gf180mcu_pdk_spice_dir.joinpath(file))}"
                f" {str(open_pdk_spice_dir.joinpath(file))}"
                for file in spice_pdk_files
            ),
            write_all,
        )
    }


#
# spice_models_python (copy inside python module)
python_models_dir = gf180mcu_local_dir.joinpath("models")
def _repl_dir(p: Path) -> Path:
    b = basename(str(p))
    return python_models_dir.joinpath(b)
python_models_srctgts = tuple(
    (file, _repl_dir(file))
    for file in spice_models_tgts
)
python_models_deps = tuple(scr for (scr, _) in python_models_srctgts)
python_models_tgts = tuple(tgt for (_, tgt) in python_models_srctgts)
def task_spice_models_python():
    """Copy SPICE models inside pdk module

    This way they can be used by pyspicefactory without needing separate
    PDK install"""
    return {
        "file_dep": python_models_deps,
        "targets": python_models_tgts,
        "actions": (
            (create_folder, (python_models_dir,)),
            *(
                f"cp {str(python_models_deps[n])} {str(python_models_tgts[n])}"
                for n in range(len(python_models_tgts))
            )
        )
    }


#
# spice
# spice task manager and tasks
spice_tm = SpiceTaskManager(
    tech_cb=get_tech, lib4name_cb=get_lib, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree, netlistfab_cb=get_netlistfab,
)

spice_export_task = spice_tm.create_export_task(
    extra_filedep=c4m_pdk_gf180mcu_py_files, extra_filedep_lib=lib_deps,
)
task_spice = spice_export_task.task_func


#
# klayout task manager and tasks
klayout_tm = KLayoutTaskManager(
    tech_cb=get_tech, lib4name_cb=get_lib, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree, gdslayers_cb=get_gdslayers,
)

klayout_dir = openpdk_tree.tool_dir(tool_name="klayout")
klayout_tech_dir = klayout_dir.joinpath("tech", "C4M.Sky130")
klayout_bin_dir = klayout_dir.joinpath("bin")
def task_copy_lyp():
    """Copy the upstream layer properties file"""
    src = gf180mcu_pdk_dir.joinpath("libs.tech", "klayout", "tech", "gf180mcu.lyp")
    tgt = klayout_tech_dir.joinpath("gf180mcu.lyp")

    return {
        "title": lambda _: "Copy the upstream layer properties file",
        "task_dep": (
            "volare",
        ),
        "file_dep": (
            src,
        ),
        "targets": (
            tgt,
        ),
        "actions": (
            (create_folder, (klayout_tech_dir,)),
            f"cp {str(src)} {str(tgt)}",
        ),
    }


# klayout export
klayout_export_task = klayout_tm.create_export_task(
    spice_params_cb=get_spiceprims, layerprops_filename="sky130.lyp",
    extra_filedep=(*c4m_pdk_gf180mcu_py_files, *pdkmaster_deps),
    extra_taskdep=("copy_lyp",)
)
task_klayout = klayout_export_task.task_func


# gds export
klayout_gds_task = klayout_tm.create_gds_task(
    extra_filedep=c4m_pdk_gf180mcu_py_files, extra_filedep_lib=lib_deps,
)
task_gds = klayout_gds_task.task_func_gds


# drc
def waive_macrodrc(lib: str, _: str, cat: ET.Element) -> bool:
    # Waive licon difftap enclosure rule in MacroLib
    # We use larger tap licon enclosure rule but these macros use the smaller
    # difftap licon enclosure rule.
    return (lib == "MacroLib") and (cat.text == "'difftap:licon asymmetric enclosure'")

klayout_drc_task = klayout_tm.create_drc_task(waive_func=waive_macrodrc)
task_drc = klayout_drc_task.task_func_drc
task_drccells = klayout_drc_task.task_func_drccells


# lvs
klayout_lvs_task = klayout_tm.create_lvs_task()
task_lvs = klayout_lvs_task.task_func_lvs
task_lvscells = klayout_lvs_task.task_func_lvscells


# sign-off
task_signoff = klayout_tm.task_func_signoff


#
# HiTAS/Yagle task maanager and tasks
def tasyag_include_cell(lib: str, cell: str):
    return (cell != "Gallery")
tasyag_cell_list: Dict[str, Collection[str]] = {
    lib: tuple(filter(lambda cell: tasyag_include_cell(lib, cell), cells))
    for lib, cells in cell_list.items()
}

tasyag_tm = TasYagTaskManager(
    tech_cb=get_tech, lib4name_cb=get_lib, cell_list=tasyag_cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree,
)


# extra SPICE init file
def task_rtl_spice_init_files():
    """Generate SPICE init files for RTL generation"""
    @dataclass
    class RTLInitSpiceInfo:
        name: str
        proc_corner: str
        init_file: Path

    infos = (
        RTLInitSpiceInfo(
            name="rtl", proc_corner="typical", init_file=tmp_dir.joinpath("init.spi"),
        ),
        *(
            RTLInitSpiceInfo(
                name=f"{lib}_nom", proc_corner="typical", init_file=tmp_dir.joinpath(f"{lib}_nom", "init.spi"),
            )
            for lib in cell_list.keys()
        ),
        *(
            RTLInitSpiceInfo(
                name=f"{lib}_slow", proc_corner="ss", init_file=tmp_dir.joinpath(f"{lib}_slow", "init.spi"),
            )
            for lib in cell_list.keys()
        ),
        *(
            RTLInitSpiceInfo(
                name=f"{lib}_fast", proc_corner="ff", init_file=tmp_dir.joinpath(f"{lib}_fast", "init.spi"),
            )
            for lib in cell_list.keys()
        ),
    )

    def gen_files(info: RTLInitSpiceInfo):
        d = info.init_file.parent
        d.mkdir(parents=True, exist_ok=True)

        spice_file = "sm141064.ngspice"
        design_file = "design.ngspice"
        with info.init_file.open("w") as f:
            f.write(dedent(f"""
                # RTL spice init
                .include "{design_file}"
                .lib "{spice_file}" {info.proc_corner}
            """[1:]))

        with open_pdk_spice_dir.joinpath(design_file).open() as fin:
            with d.joinpath(design_file).open("w") as fout:
                for line in fin:
                    fout.write(line)

        with open_pdk_spice_dir.joinpath(spice_file).open() as fin:
            with d.joinpath(spice_file).open("w") as fout:
                for line in fin:
                    pat = "v\([^\)]*\)"
                    s = re.search(pat, line)
                    while s:
                        line = line[:s.start()] + "0.0" + line[s.end():]
                        s = re.search(pat, line)
                    fout.write(line)

    for info in infos:
        d = info.init_file.parent
        design_file = d.joinpath("design.ngspice")
        sm_file = d.joinpath("sm141064.ngspice")
        yield {
            "title": (lambda _: f"Spice init model file for {info.name}"),
            "name": info.name,
            "task_dep": ("spice_models",),
            "targets": (
                info.init_file, design_file, sm_file,
            ),
            "actions": (
                (gen_files, (info,)),
            )
        }


# rtl
rtl_tm = tasyag_tm.create_rtl_task(
    work_dir=tmp_dir, override_dir=override_dir,
    spice_model_files=(tmp_dir.joinpath("init.spi"),),
)
task_rtl = rtl_tm.task_func


# liberty
liberty_corner_data: LibertyCornerDataT = {
    "StdCellLib": {
        "nom": (3.30, 25, (tmp_dir.joinpath("StdCellLib_nom", "init.spi"),)),
        "fast": (3.63, -20, (tmp_dir.joinpath("StdCellLib_fast", "init.spi"),)),
        "slow": (2.97, 85, (tmp_dir.joinpath("StdCellLib_slow", "init.spi"),)),
    },
    "StdCell5V0Lib": {
        "nom": (5.00, 25, (tmp_dir.joinpath("StdCell5V0Lib_nom", "init.spi"),)),
        "fast": (5.50, -20, (tmp_dir.joinpath("StdCell5V0Lib_fast", "init.spi"),)),
        "slow": (4.50, 85, (tmp_dir.joinpath("StdCell5V0Lib_slow", "init.spi"),)),
    },
}

liberty_tm = tasyag_tm.create_liberty_task(
    work_dir=tmp_dir, corner_data=liberty_corner_data,
)
task_liberty = liberty_tm.task_func


#
# klayout
klayout_dir = openpdk_tree.tool_dir(tool_name="klayout")
klayout_tech_dir = klayout_dir.joinpath("tech", "C4M.GF180MCU")
klayout_bin_dir = klayout_dir.joinpath("bin")


#
# coriolis
def task_coriolis():
    """Generate coriolis support files"""

    coriolis_dir = openpdk_tree.tool_dir(tool_name="coriolis")
    corio_dir = coriolis_dir.joinpath("techno", "etc", "coriolis2")
    corio_node180_dir = corio_dir.joinpath("node180")
    corio_gf180mcu_dir = corio_node180_dir.joinpath("gf180mcu")

    corio_nda_init_file = corio_dir.joinpath("__init__.py")
    corio_node130_init_file = corio_node180_dir.joinpath("__init__.py")
    corio_gf180mcu_init_file = corio_gf180mcu_dir.joinpath("__init__.py")
    corio_gf180mcu_techno_file = corio_gf180mcu_dir.joinpath("techno.py")
    corio_gf180mcu_lib_files = tuple(
        corio_gf180mcu_dir.joinpath(f"{lib}.py") for lib in cell_list.keys()
    )

    def gen_init():
        from c4m.pdk import gf180mcu

        with corio_gf180mcu_init_file.open("w") as f:
            print("from .techno import *", file=f)
            for lib in gf180mcu.libs:
                print(f"from .{lib.name} import setup as {lib.name}_setup", file=f)

            print(
                "\n__lib_setups__ = [{}]".format(
                    ",".join(f"{lib.name}.setup" for lib in gf180mcu.libs)
                ),
                file=f,
            )

    def gen_coriolis():
        from pdkmaster.io import coriolis as _iocorio
        from c4m.flexcell import coriolis_export_spec
        from c4m.pdk import gf180mcu

        expo = _iocorio.FileExporter(
            tech=gf180mcu.tech, gds_layers=gf180mcu.gds_layers, spec=coriolis_export_spec,
        )

        with corio_gf180mcu_techno_file.open("w") as f:
            f.write(dedent("""
                # Autogenerated file
                # SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
            """))
            f.write(expo())

        for lib in gf180mcu.libs:
            with corio_gf180mcu_dir.joinpath(f"{lib.name}.py").open("w") as f:
                f.write(expo(lib))

    return {
        "title": lambda _: "Creating coriolis files",
        "file_dep": (
            *c4m_pdk_gf180mcu_py_files,
            *pdkmaster_deps, *flexcell_deps, #*flexio_deps, *flexmem_deps,
        ),
        "targets": (
            corio_nda_init_file, corio_node130_init_file, corio_gf180mcu_init_file,
            corio_gf180mcu_techno_file, *corio_gf180mcu_lib_files,
        ),
        "actions": (
            (create_folder, (corio_gf180mcu_dir,)),
            corio_nda_init_file.touch, corio_node130_init_file.touch,
            gen_init, gen_coriolis,
        ),
    }


#
# release
def task_tarball():
    """Create a tarball"""
    from datetime import datetime

    tarballs_dir = top_dir.joinpath("tarballs")
    t = datetime.now()
    tarball = tarballs_dir.joinpath(f'{t.strftime("%Y%m%d_%H%M")}_c4m_pdk_gf180mcu.tgz')

    return {
        "title": lambda _: "Create release tarball",
        "task_dep": (
            "coriolis", "klayout", "spice_models", "spice", "gds", "rtl", "liberty",
        ),
        "targets": (tarball,),
        "actions": (
            (create_folder, (tarballs_dir,)),
            f"cd {str(top_dir)}; tar czf {str(tarball)} open_pdk",
        )
    }
def task_tarball_nodep():
    """Create a tarball from existing open_pdk"""
    from datetime import datetime

    tarballs_dir = top_dir.joinpath("tarballs")
    t = datetime.now()
    tarball = tarballs_dir.joinpath(f'{t.strftime("%Y%m%d_%H%M")}_nd_c4m_pdk_gf180mcu.tgz')

    return {
        "title": lambda _: "Create release tarball",
        "targets": (tarball,),
        "actions": (
            (create_folder, (tarballs_dir,)),
            f"cd {str(top_dir)}; tar czf {str(tarball)} open_pdk",
        )
    }
