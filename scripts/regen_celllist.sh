#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
# This script assumes it is being called from the top source directory

# We start from a clean plate, avoiding errors when a library name changes
echo 'StdCellLib: {}' >cell_list.yml
echo 'StdCell5V0Lib: {}' >>cell_list.yml
doit cell_list
